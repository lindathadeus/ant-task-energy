package com.lin.tester;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.lin.sched.Constant;
/**
 * TSP solved by Brute force Method
 * @author linda
 *
 */
public class TSPBrute{

	public static int nTasks = Constant.nTasks;
	public static int nVms = Constant.nVms;

	private static int nNodes=Constant.nNodes;//no. of cities
	private static double[][] d;
	private static int bestMakeSpan=Integer.MAX_VALUE;
	private static ArrayList<Integer> bestPath = new ArrayList<Integer>(nNodes);
	private static int rec_calls=0;
	private static int iterations;
	
   
    public static void permute(ArrayList<Integer> a) {
        ArrayList<Integer> sub = new ArrayList<Integer>();
        permute(sub, a);
    }

    public static void permute(ArrayList<Integer> sub, ArrayList<Integer> a) {
		
    	//count the no. of times the function is called
    	rec_calls++;
    	
	     int L = a.size();
	     if (L == 0){ 
	    	 //Count of the no. of times the solution is constructed
	    	 iterations++;
	    	 System.out.println("---------------------------");
	    	 System.out.print(getIterations()+" - "+sub);
	    	 System.out.println(" Cost: "+getPathCost(sub));
	    	 System.out.println("---------------------------");
	     }
	     else {
         //System.out.println(sub);
	    	 for (int i = 0; i < L; i++) {
	             ArrayList<Integer> ab = new ArrayList<Integer>(sub);
	             ab.add(a.get(i));
	             ArrayList<Integer> bc = new ArrayList<Integer>(a);
	             bc.remove(i);
	             permute(ab, bc);
	         }
	     }
    }


    public static void main(String[] args) {
       
    	//overall printing comment to print on console
    	//Constant.printToFile("brute.txt");
    	
    	System.out.println("Brute Force Approach Program for TSP\n");
    	
    	System.out.println("The Program Starts Execution....");
    	
       ArrayList<Integer> nodes = new ArrayList<Integer>(nTasks);
       
       for(int i=0;i<nTasks;i++){
    	   nodes.add(i, i);
       }
    	   
       //load cost matrix
       loadCostMatrix();
       
       //perform brute force cost calculation
       permute(nodes);
       
       //best path
       System.out.println("\nBrute-Force Approach" +
       		"\nBest Path: "+bestPath+"\nIts Makespan: "+getBestMakeSpan());
       
      //print gantt chart
     //  for(int i=0;i<Constant.nVms;i++)
    	//   System.out.println("\nIn VM"+i+"Total Completion Time for Tasks="+taskCompletionTimeInVm[i]);
    	   
       System.out.println("Total Iterations: "+getIterations());
       System.out.println("Total Recursive Calls: "+rec_calls);
       System.out.println("The Program successfully finished its Execution!!!");
    }
    
    public static void loadCostMatrix(){
    	
		double et[][]=new double[nNodes][nNodes];
	 //Reading ET Matrix from the file
        int et_i=0,et_j=0;
        try{
        	File et_file = new File(Constant.path+"ET_Tasks_pr" + nNodes + ".txt");
        	String each_row="";
        	BufferedReader br = new BufferedReader(new FileReader(et_file));
        	
        	while((each_row=br.readLine()) != null){
        		String et_values[]=each_row.split(","); 
        		List<String> list = Arrays.asList(et_values);
        		//Collections.reverse(list);
        		for(et_j=0;et_j<list.size();et_j++)
        			et[et_i][et_j]=Double.parseDouble(list.get(et_j));
        		et_i++;
        	}
        	
        	br.close();
        	
        }catch(Exception e){
        	e.printStackTrace();
        }
        
        System.out.println("ET Matrix");
        for(int i = 0; i < nNodes; i++){
            for(int j = 0; j < nVms; j++)
            	System.out.print(et[i][j]+" ");
            System.out.println(" ");
        }
       
        
        double max[] = new double[nVms];
        //initializing the max
        for(int i=0;i<nVms;i++)
        	max[i]=0;
        //take the max values for first round
        for(int i=0;i<nTasks;i++)
        	for(int j=0;j<nVms;j++)
        		if(et[i][j]>max[j])
        			max[j]=et[i][j];
        
        //calculation of distance cum ETC Matrix and EET Matrix Approach 
        d = new double[nNodes][nNodes];
        
        System.out.println("\nETC Matrix");
        for(int i=0;i<nNodes;i++){
        	for(int j=0;j<nNodes;j++)
        	{
        		//int round_j=j/nVms;
        		d[i][j]=et[i][j%nVms];//*/+max[j%nVms]*round_j;
        		System.out.print(d[i][j]+" ");
        	}
        	System.out.println("\n");
        }
        
    }
    
    public static int getPathCost(ArrayList<Integer> path){
    	
    	int pathCost=0,makeSpan=0;
    	int[] taskCompletionTimeInVm = new int[Constant.nVms];
    	int i=0;
    	for(i=0;i<path.size()-1;i++){
    		pathCost+=d[path.get(i)][path.get(i+1)];
    		taskCompletionTimeInVm[(path.get(i+1))%Constant.nVms]+=d[path.get(i)][path.get(i+1)];
    		//System.out.println("path="+d[path.get(i)][path.get(i+1)]+"overall="+taskCompletionTimeInVm[(i+1)%nVms]);
    	}
    	pathCost+=d[path.get(path.size()-1)][path.get(0)];
    	taskCompletionTimeInVm[(path.get(0))%Constant.nVms]+=d[path.get(path.size()-1)][path.get(0)];
    	//System.out.println("path="+d[path.get(path.size()-1)][path.get(0)]+"overall="+taskCompletionTimeInVm[(path.get(0))%nVms]);
    	//print gantt chart
    //	for(int k=0;k<nVms;k++)
    //		System.out.println("Task Com in VM"+k+"="+taskCompletionTimeInVm[k]);
    	
    	//calculate makespan -> max of task Completion time
    	int max=0;
    	for(int k=0;k<Constant.nVms;k++)
    		if(max<taskCompletionTimeInVm[k])
    			max=taskCompletionTimeInVm[k];
    	makeSpan=max;
    	
    	System.out.print(" Makespan="+makeSpan);
    	
    	
    	
    	//update best path
    	if(bestMakeSpan>makeSpan){
    		
    			bestMakeSpan=makeSpan;
    			
	    		//bestCost=pathCost;
	    		bestPath=path;
    		
    	}
    	
    	return pathCost;
    }
    
    public static ArrayList<Integer> getBestPath(){
    	return bestPath;
    }
    
    public static int getBestMakeSpan(){
    	return bestMakeSpan;
    }
    
    public static int getIterations(){
    	return iterations;
    }

    
}

