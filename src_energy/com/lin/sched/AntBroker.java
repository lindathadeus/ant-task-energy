package com.lin.sched;
import org.cloudbus.cloudsim.DatacenterBroker;


public class AntBroker extends DatacenterBroker{

	//1,3,2,0,4 ---> best path without makespan approach
	//2, 1, 4, 0, 3, 1, 1, 0, 0, 1
	//5, 0, 9, 2, 8, 1, 7, 2, 6, 1, 4, 0, 3, 2, 2, 1, 1, 0, 0, 2


	private static int[] pathNode = {2, 1, 4, 1, 3, 1, 1, 1, 0, 1};
	

	public AntBroker(String name) throws Exception {
		super(name);
		// TODO Auto-generated constructor stub
	}

	//schedule function
	public void scheduleTaskstoVms(){
		
		int reqTasks=cloudletList.size();
		int reqVms=vmList.size();
    	//bind the cloudlets to the vms. This way, the broker
    	// will submit the bound cloudlets only to the specific VM
    	//based on the path array
    	for(int i=0;i<2*reqTasks;i+=2){
    		System.out.println(pathNode[i]+", "+pathNode[i+1]+" = "+pathNode[i]+", "+(pathNode[i+1]));
    		bindCloudletToVm(pathNode[i], pathNode[i+1]);
    	}
		
    	//bindCloudletToVm(pathNode[reqTasks-1], (pathNode[0]%reqVms));
    	//System.out.println(pathNode[reqTasks-1]+","+(pathNode[0]%reqVms));
    	
	}
}
