package com.lin.sched;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

public class Constant {
	
	//relative path windows=".\\output\\"  --> in the current parent directory
	//linux="./output/"
	
	public static String path= ".\\output\\" ;
	public static int nTasks=5;
	public static int nHosts=2;
	public static int nVms = 2;
	public static int nNodes = nTasks;
	
	public static void printToFile(String filename) {
	    
		
	       try {
			PrintStream out = new PrintStream(new FileOutputStream(path+filename));
			System.setOut(out);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	       
	}
}
