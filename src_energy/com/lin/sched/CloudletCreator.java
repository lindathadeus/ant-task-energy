package com.lin.sched;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.UtilizationModel;
import org.cloudbus.cloudsim.UtilizationModelFull;

/**
 * CloudletCreator Creates Cloudlets as per the User Requirements.
 * @author Linda J
 *
 */
public class CloudletCreator {
	
	
	//cloudlet creator
	public ArrayList<Cloudlet> createUserCloudlet(int reqTasks,int brokerId){
		ArrayList<Cloudlet> cloudletList = new ArrayList<Cloudlet>();
		
    	//Cloudlet properties
    	int id = 0;
    	int pesNumber=1;
    	long length = 100000;
    	long fileSize = 300;
    	long outputSize = 300;
    	UtilizationModel utilizationModel = new UtilizationModelFull();
    	
    	List<Integer> order =new ArrayList<Integer>();
    	for(int i=1;i<=reqTasks;i++)order.add(i); //Collections.shuffle(order);
    	
    	//descending order
    	//Collections.reverse(order);
    	
    	for(id=0;id<reqTasks;id++){
    		long random = order.get(id) ;
    		Cloudlet task = new Cloudlet(id, (length*random), pesNumber, fileSize, outputSize, utilizationModel, utilizationModel, utilizationModel);
    		task.setUserId(brokerId);
    		//taskLengthList.add(length*(reqTasks-id));
    		
    		System.out.println("Task"+task.getCloudletId()+" length="+task.getCloudletLength() );
    	
    		
    		//add the cloudlets to the list
        	cloudletList.add(task);
    	}

    	System.out.println("SUCCESSFULLY Cloudletlist created :)");

		return cloudletList;
		
	}

}
