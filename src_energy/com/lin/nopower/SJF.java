package com.lin.nopower;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.List;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.Datacenter;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.core.CloudSim;
import com.lin.sched.*;

/**
 * SJF Task scheduling
 * @author Linda J
 *
 */
public class SJF {

	/** The cloudlet list. */
	private static List<Cloudlet> cloudletList;

	/** The vmlist. */
	private static List<Vm> vmlist;

	private static int reqTasks = Constant.nTasks;
	private static int reqVms = Constant.nVms;
	//private static List<Long> taskLengthList = new ArrayList<Long>(); 
	
	/**
	 * Creates main() to run this example
	 */
	public static void main(String[] args) {
		
		//print to file
		Constant.printToFile("sjf.txt");

		Log.printLine("Starting SJF Based Task scheduling...");

	        try {
	        	// First step: Initialize the CloudSim package. It should be called
	            	// before creating any entities.
	            	int num_user = 1;   // number of cloud users
	            	Calendar calendar = Calendar.getInstance();
	            	boolean trace_flag = false;  // mean trace events

	            	// Initialize the CloudSim library
	            	CloudSim.init(num_user, calendar, trace_flag);

	            	// Second step: Create Datacenters
	            	//Datacenters are the resource providers in CloudSim. We need at list one of them to run a CloudSim simulation
	            	@SuppressWarnings("unused")
	            	Datacenter d[]= new Datacenter[reqVms];
	            	
	            	for(int i=0;i<reqVms;i++)
	            		d[i]=createDatacenter("Datacenter_"+i);
	   
	            	//Third step: Create Broker
	            	SjfBroker broker = createBroker();
	            	int brokerId = broker.getId();

	            	//Fourth step: Create list of virtual machines
	            	vmlist = new VmsCreator().createRequiredVms(reqVms,brokerId);
	            	
            	
	            	//submit vm list to the broker
	            	broker.submitVmList(vmlist);


	            	//Fifth step: Create list of Cloudlets
	            	cloudletList = new CloudletCreator().createUserCloudlet(reqTasks,brokerId);

            	
	            	
	            	//submit cloudlet list to the broker
	            	broker.submitCloudletList(cloudletList);
	            	
	            	
	            	//create the execution time matrix
	            	double[][] et= new double[cloudletList.size()][vmlist.size()]; 
	            	
	            	//calculate the execution time matrix
	            	try{
	            		
	            		File et_file = new File(Constant.path+"ET_" + cloudletList.size() +"x"+vmlist.size()+ ".txt");
	            		if(!et_file.exists())
	            			et_file.createNewFile();
	            		PrintStream out_pr_st = new PrintStream(new FileOutputStream(et_file));
	            		
	            		for(int i=0;i<cloudletList.size();i++){
		            		for(int j=0;j<vmlist.size();j++){
		            			et[i][j]= cloudletList.get(i).getCloudletLength()/vmlist.get(j).getMips();
		            			System.out.print(" "+et[i][j]);
		            			
		            			out_pr_st.write((et[i][j]+"").getBytes());
		            			if(j<(vmlist.size()-1)) out_pr_st.write((",").getBytes());
		            		}
		            		System.out.print("\n");
		            		//out_pr_st.print("\n");
		            		out_pr_st.println("");
		            	}
	            		
	            		out_pr_st.close();

	            		
	            	}catch(Exception ex){
	            		ex.printStackTrace();
	            	}
	            		            
	            	
	            	//call schedule function of broker
	            	broker.scheduleTaskstoVms();
	            	
	            	// Sixth step: Starts the simulation
	            	CloudSim.startSimulation();


	            	// Final step: Print results when simulation is over
	            	List<Cloudlet> newList = broker.getCloudletReceivedList();

	            	CloudSim.stopSimulation();

	            	printCloudletList(newList);

	            	Log.printLine("SJF finished!");
	        }
	        catch (Exception e) {
	            e.printStackTrace();
	            Log.printLine("The simulation has been terminated due to an unexpected error");
	        }
	    }

		private static Datacenter createDatacenter(String name){

			Datacenter datacenter=new DataCenterCreator().createUserDatacenter(name, reqVms);			

	        return datacenter;
	    }

	    //We strongly encourage users to develop their own broker policies, to submit vms and cloudlets according
	    //to the specific rules of the simulated scenario
	    private static SjfBroker createBroker(){

	    	SjfBroker broker = null;
	        try {
				broker = new SjfBroker("Broker");
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		    return broker;
	    }

	    /**
	     * Prints the Cloudlet objects
	     * @param list  list of Cloudlets
	     */
	    private static void printCloudletList(List<Cloudlet> list) {
	        int size = list.size();
	        Cloudlet cloudlet;

	        String indent = "    ";
	        Log.printLine();
	        Log.printLine("========== OUTPUT ==========");
	        Log.printLine("Cloudlet ID" + indent + "STATUS" + indent +
	                "Data center ID" + indent + "VM ID" + indent + "Time" + indent + "Start Time" + indent + "Finish Time");

	        DecimalFormat dft = new DecimalFormat("###.##");
	        for (int i = 0; i < size; i++) {
	            cloudlet = list.get(i);
	            Log.print(indent + cloudlet.getCloudletId() + indent + indent);

	            if (cloudlet.getCloudletStatus() == Cloudlet.SUCCESS){
	                Log.print("SUCCESS");

	            	Log.printLine( indent + indent + cloudlet.getResourceId() + indent + indent + indent + cloudlet.getVmId() +
	                     indent + indent + dft.format(cloudlet.getActualCPUTime()) + indent + indent + dft.format(cloudlet.getExecStartTime())+
                             indent + indent + dft.format(cloudlet.getFinishTime()));
	            }
	        }

	    }
}
