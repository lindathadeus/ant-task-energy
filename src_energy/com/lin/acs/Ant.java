/**
 * Ant.java
 *
 * @author Linda J
 */

package com.lin.acs;

import java.util.*;
import java.awt.font.GraphicAttribute;
import java.io.*;

import com.lin.sched.Constant;
import com.lin.tester.Bipartite;

public class Ant extends Observable implements Runnable
{
    protected int m_nAntID;
    
    protected int[][]  m_path;
    protected int      m_nCurNode;
    protected int      m_nStartNode;
    protected int 	   m_nEndNode; // end node
    protected double   m_dPathValue;
    protected double   m_dPathMakeSpanValue; // path's makespan value
    private double[]   m_dTaskCompletionTimeInVm; // Task Completion time in Vm
    
    protected Observer m_observer;
    protected Vector   m_pathVect;
    
    private static int s_nAntIDCounter = 0;
    private static PrintStream s_outs;
    
    protected static AntColony s_antColony;
    
    public static double    s_dBestPathValue = Double.MAX_VALUE;
    public static double    s_dBestMakeSpanValue = Double.MAX_VALUE; // best makespan value
    public static Vector    s_bestPathVect  = null;
    public static int[][]   s_bestPath      = null;
    public static int       s_nLastBestPathIteration = 0;
                        
    
    //Ant for TSP Variables
    private static final double Beta = -2;
    private static final double Gamma = -1;
    private static final double Q0   = 0.9;
    private static final double R    = 0.1;
    
    private static final Random s_randGen = new Random(System.currentTimeMillis());
        
    protected Hashtable m_nodesToVisitTbl, m_nodesToSelectTbl;

    public static void setAntColony(AntColony antColony)
    {
        s_antColony = antColony;
    }
    
    public static void reset()
    {
        s_dBestPathValue = Double.MAX_VALUE;
        s_dBestMakeSpanValue = Double.MAX_VALUE;
                
        s_bestPathVect = null;
        s_bestPath = null;
        s_nLastBestPathIteration = 0;
        s_outs = null;
    }
    
    public Ant(int nStartNode, Observer observer)
    {
        s_nAntIDCounter++;
        m_nAntID    = s_nAntIDCounter;
        m_nStartNode = nStartNode;
        m_observer  = observer;
    }

    public void init()
    {
        if(s_outs == null)
        {
            try
            {
                s_outs = new PrintStream(new FileOutputStream(Constant.path + s_antColony.getID()+ "_" + s_antColony.getGraph().tNodes() + "x" + s_antColony.getAnts() + "x" + s_antColony.getIterations() + "_ants.txt"));
            }
            catch(Exception ex)
            {
                ex.printStackTrace();
            }
        }

        final AntGraph graph = s_antColony.getGraph();
        m_nCurNode   = m_nStartNode;        
        m_path      = new int[graph.tNodes()][graph.vNodes()];        
        m_pathVect  = new Vector((graph.tNodes() * 2) ); //path consisting of task nodes and vnodes pairs        
        m_pathVect.addElement(new Integer(m_nStartNode));
        m_dPathValue = 0;
        m_dPathMakeSpanValue = 0;
        m_dTaskCompletionTimeInVm = new double[Constant.nVms];
        
        m_nodesToVisitTbl = new Hashtable(graph.tNodes());
        m_nodesToSelectTbl = new  Hashtable(graph.vNodes());

        for(int i = 0; i < graph.tNodes(); i++)
           m_nodesToVisitTbl.put(new Integer(i), new Integer(i));

        for(int i = 0; i < graph.vNodes(); i++)
           m_nodesToSelectTbl.put(new Integer(i), new Integer(i));
           
        m_nodesToVisitTbl.remove(new Integer(m_nStartNode));
            
     }

    public void start()
    {
        init();
        Thread thread = new Thread(this);
        thread.setName("Ant " + m_nAntID);
        thread.start();
    }
    
	public void run()
    {
        final AntGraph graph = s_antColony.getGraph();
        
        int selectedVNode, newTNode;
        
        //print the start node
        //System.out.println("Ant " + m_nAntID+" startnode"+m_nStartNode);
        
        //reset the totaltask completion time in vms
        m_dTaskCompletionTimeInVm = new double[Constant.nVms];
        // repeat while End of Activity Rule returns false
        //while there still exists a task node, the tasks nodes table to visit is not empty
        while(!end())
        {
            
          // synchronize the access to the graph
            synchronized(graph)
            {               
            	                        	
                // apply the State Transition Rule for selecting the vm node or vnode
            	selectedVNode = vmRule(m_nCurNode);
            	
            	//System.out.println("Just selected the VNode, Ant " + m_nAntID+" VM["+selectedVNode+"]="+m_dTaskCompletionTimeInVm[selectedVNode]);
                
            	// apply task rule to find the next task node
                newTNode = taskRule(m_nCurNode);
                
                // update the length of the path
                m_dPathValue += graph.delta(m_nCurNode, selectedVNode);
                
                //print the value of taskCompletiontime in Vm before updation
                //System.out.println("before updation, Ant " + m_nAntID+" VM["+selectedVNode+"]="+m_dTaskCompletionTimeInVm[selectedVNode]);
                
                // update the vm's total processing time table
                m_dTaskCompletionTimeInVm[selectedVNode /*% Constant.nVms*/] += graph.delta(m_nCurNode, selectedVNode);
                
                //print the task node too
                //System.out.println("Task "+m_nCurNode+"is assigned to vnode"+selectedVNode);
                
                //System.out.println("Ant " + m_nAntID+" VM["+selectedVNode+"]="+m_dTaskCompletionTimeInVm[selectedVNode]);
            }
                        
            // add the current node the list of visited nodes
            // update the current node, here take the next node from the task nodes to visit list 
            m_pathVect.addElement(new Integer(selectedVNode));
            m_pathVect.addElement(new Integer(newTNode));
            
            m_path[m_nCurNode][selectedVNode] = 1;
                        
            synchronized(graph)
            {
                // apply the Local Updating Rule
                localUpdatingRule(m_nCurNode, selectedVNode);
            }
            
            m_nCurNode  = newTNode;
        }
        
        //linda enhancements for makespan
        synchronized(graph)
        {
	        //once the end is reached assign the endnode
	        m_nEndNode = m_nCurNode;
       
             //vm for final task
            // apply the State Transition Rule for selecting the vm node or vnode
        	selectedVNode = vmRule(m_nEndNode);
        	
            // update the vm's total processing time table
            m_dTaskCompletionTimeInVm[selectedVNode /*% Constant.nVms*/] += graph.delta(m_nCurNode, selectedVNode);
            
            
            //print the values
            //System.out.println("Ant " + m_nAntID+" VM["+selectedVNode+"]="+m_dTaskCompletionTimeInVm[selectedVNode]);
            
        	// add the selected VMnode to the path vector
        	m_pathVect.addElement(new Integer(selectedVNode));
        	
        	 //calculate makespan -> max of task Completion time
        	double max=0;
        	for(int k=0;k<Constant.nVms;k++)
        		if(max<m_dTaskCompletionTimeInVm[k])
        			max=m_dTaskCompletionTimeInVm[k];
        	m_dPathMakeSpanValue=max;
	    }
        synchronized(graph)
        {
            // update the best tour value
        	//instead pathvalue, we should give
        	//makespan value
            if(better(m_dPathMakeSpanValue, s_dBestMakeSpanValue))
            {
                s_dBestPathValue        = m_dPathValue;
                //update makespan
                s_dBestMakeSpanValue    = m_dPathMakeSpanValue;
                s_bestPath              = m_path;
                s_bestPathVect          = m_pathVect;
                s_nLastBestPathIteration = s_antColony.getIterationCounter();
                
                s_outs.println("Ant + " + m_nAntID + "," + s_dBestPathValue + "," + s_nLastBestPathIteration + "," + s_bestPathVect.size() + "," + s_bestPathVect);
            }
        }
        
        // update the observer
        m_observer.update(this, null);
        
        if(s_antColony.done())
            s_outs.close();
 
        // print the task completion time
       // for(int i=0;i<Constant.nVms;i++)
        //	System.out.println("Ant " + m_nAntID+" VM["+i+"]="+m_dTaskCompletionTimeInVm[i]);
    }
    
    protected boolean better(double dPathValue, double dBestPathValue){
    	return dPathValue < dBestPathValue;
    }
    
    //select the vnode to process
    public int vmRule(int nCurNode)
    {
        final AntGraph graph = s_antColony.getGraph();
        
        // generate a random number
        double q    = s_randGen.nextDouble();
        int nMaxNode = -1;
       
        //check if vm nodes select list is empty, and if so, add the vm nodes to it, for the next round.
        if(m_nodesToSelectTbl.isEmpty()){
        	// reset the select table of an ant here
            for(int i = 0; i < graph.vNodes(); i++)
                m_nodesToSelectTbl.put(new Integer(i), new Integer(i));

        }
        
        if(q <= Q0)  // Exploitation
        {
   
            double dMaxVal = -1;
            double dVal;
            int nNode;
            
            // search the max of the value as defined in Eq. a)
            //here we have to include the nodes to select table
            Enumeration enum1 = m_nodesToSelectTbl.elements();
            
            while(enum1.hasMoreElements())
            {
                // select a node
                nNode = ((Integer)enum1.nextElement()).intValue();
                
                // check on tau
                if(graph.tau(nCurNode, nNode) == 0)
                    throw new RuntimeException("tau = 0");
                
                // get the value
                //dVal = graph.tau(nCurNode, nNode) * Math.pow(graph.etha(nCurNode, nNode), Beta);
                //dVal = graph.tau(nCurNode, nNode) * Math.pow(calculateMakeSpan(nCurNode,nNode,m_pathVect, graph), Beta);
                dVal = graph.tau(nCurNode, nNode) * 
                		Math.pow(calculateMakeSpan(nCurNode,nNode,m_pathVect, graph), Beta)
                		* Math.pow(calculatePowerInHosts(nCurNode, nNode, m_pathVect, graph), Gamma);
                
                //check power parameter
                

                // check if it is the max
                if(dVal > dMaxVal)
                {
                    dMaxVal  = dVal;
                    nMaxNode = nNode;
                }
            }
        }
        else  // Exploration
        {
            double dSum = 0;
            int nNode = -1;
            
            // get the sum at denominator
            Enumeration enum1 = m_nodesToSelectTbl.elements();
            while(enum1.hasMoreElements())
            {
                nNode = ((Integer)enum1.nextElement()).intValue();
                if(graph.tau(nCurNode, nNode) == 0)
                    throw new RuntimeException("tau = 0");
                
                // Update the sum
               //dSum += graph.tau(nCurNode, nNode) * Math.pow(graph.etha(nCurNode, nNode), Beta);
               //dSum += graph.tau(nCurNode, nNode) * Math.pow(calculateMakeSpan(nCurNode,nNode, m_pathVect, graph), Beta);
                dSum += graph.tau(nCurNode, nNode) * 
                		Math.pow(calculateMakeSpan(nCurNode,nNode, m_pathVect, graph), Beta)
                * Math.pow(calculatePowerInHosts(nCurNode, nNode, m_pathVect, graph), Gamma);
              
            }
            
            if(dSum == 0)
                throw new RuntimeException("SUM = 0");
            
            // get the everage value
            double dAverage = dSum / (double)m_nodesToSelectTbl.size();
            
            // search the node in agreement with eq. Beta)
            enum1 = m_nodesToSelectTbl.elements();
            while(enum1.hasMoreElements() && nMaxNode < 0)
            {
                nNode = ((Integer)enum1.nextElement()).intValue();
                
                // get the value of p as defined in eq. Beta)
                // calculateMakeSpan(nNode, graph.delta(nCurNode, nNode))
                //graph.etha(nCurNode, nNode)
                double p =
                		//(graph.tau(nCurNode, nNode) * Math.pow(graph.etha(nCurNode, nNode), Beta)) / dSum;
                    //(graph.tau(nCurNode, nNode) * Math.pow(calculateMakeSpan(nCurNode,nNode ,m_pathVect, graph), Beta)) / dSum;
                		(
                				graph.tau(nCurNode, nNode) *
                				Math.pow(calculateMakeSpan(nCurNode,nNode ,m_pathVect, graph), Beta)
                				* Math.pow(calculatePowerInHosts(nCurNode, nNode, m_pathVect, graph),Gamma)
                		) / dSum;
               
                // if the value of p is greater the the average value the node is good
                //if((graph.tau(nCurNode, nNode) * Math.pow(calculateMakeSpan(nCurNode,nNode,  m_pathVect, graph), Beta)) > dAverage)
                //if((graph.tau(nCurNode, nNode) * Math.pow(graph.etha(nCurNode, nNode), Beta)) > dAverage)
                if( (
        				graph.tau(nCurNode, nNode) *
        				Math.pow(calculateMakeSpan(nCurNode,nNode ,m_pathVect, graph), Beta)
        				* Math.pow(calculatePowerInHosts(nCurNode, nNode, m_pathVect, graph),Gamma)
        		    )
        		>dAverage )
                {
                    //System.out.println("Found");
                    nMaxNode = nNode;
                }
            }
            
            if(nMaxNode == -1)
                nMaxNode = nNode;
       }
                 
        if(nMaxNode < 0)
            throw new RuntimeException("maxNode = -1");
        
        // delete the selected node from the list of node to visit
        m_nodesToSelectTbl.remove(new Integer(nMaxNode));
        
        return nMaxNode;
    }
      
    public int taskRule(int nCurNode){
    	
    	int nMaxNode = -1, nNode;
    	
        @SuppressWarnings("rawtypes")
		Enumeration enum1 = m_nodesToVisitTbl.elements();
  	
        while(enum1.hasMoreElements() && nMaxNode < 0)
        {
            nNode = ((Integer)enum1.nextElement()).intValue();
            nMaxNode = nNode;
        }
        
        if(nMaxNode < 0)
            throw new RuntimeException("maxNode = -1, Nodes to visit Table is Empty!!!");
        
        // delete the selected node from the list of node to visit
        m_nodesToVisitTbl.remove(nMaxNode);
        
        return nMaxNode;
    	
    }

    public void localUpdatingRule(int nCurNode, int nNextNode)
    {
        final AntGraph graph = s_antColony.getGraph();
        
        // get the value of the Eq. c)
        double val =
            ((double)1 - R) * graph.tau(nCurNode, nNextNode) +
            (R * (graph.tau0()));
        
        // update tau
        graph.updateTau(nCurNode, nNextNode, val);
    }
     
    //block for getting access for graph's delta
    //it is for estimating makespan
    public double calculateMakeSpan(int currentTNode, int selectedVNode, Vector oldPath, AntGraph graph){
  	  
    	double makeSpan=0.0;
        double[] taskCompletionTimeInVm = new double[Constant.nVms]; // Task Completion time in Vm
        Object[] oldPathArray = oldPath.toArray();
        int size = oldPath.size();
        int[] newPathEstimate = new int[size+1];
        
        //System.out.print("\nPath tobe traced is ");
        for(int i=0;i<size;i++){
        	newPathEstimate[i]=(int) oldPathArray[i];
        	//System.out.print(" "+newPathEstimate[i]);
        }
        newPathEstimate[size]=selectedVNode;
        //System.out.print(" "+newPathEstimate[size]);
      
        //System.out.println("For Path"+oldPath+" & "+selectedVNode);
      //assign the taskCompletion time
        for(int i=0;i<(size+1);i+=2){
        	double executiontime = graph.delta(newPathEstimate[i], newPathEstimate[i+1]);
        	taskCompletionTimeInVm[newPathEstimate[i+1]]+= executiontime;
        	//System.out.println("VM["+newPathEstimate[i+1]+"]: "+taskCompletionTimeInVm[newPathEstimate[i+1]]);
        	//System.out.println("ET:"+newPathEstimate[i]+","+ newPathEstimate[i+1]+":"+executiontime);
        }
        
   	    //calculate makespan -> max of task Completion time
        double max=0;
	   	for(int k=0;k<Constant.nVms;k++)
	   		if(max<taskCompletionTimeInVm[k])
	   			max=taskCompletionTimeInVm[k];
	   	makeSpan=max;
	   	
	   	System.out.println("For Path"+oldPath+" & "+selectedVNode+" Makespan: "+makeSpan);
	   	return makeSpan;
    	
    }
    
    //block for calculating total power consumed 
    //in the hosts after allocation of tasks to the
    //vms running on the hosts
    public double calculatePowerInHosts (int currentTNode, int selectedVNode, Vector oldPath, AntGraph graph){
    	double powerConsumed=0.0;
    	double P_max = 250.0, P_idle = 0.7 * P_max;
    	
    	double utilizationOfHost[], inHostPower[] = new double[Constant.nHosts];
    	
    	//assign utilization list of hosts
    	utilizationOfHost=Bipartite.u;
    	
    	for(int i=0;i<utilizationOfHost.length;i++){
    		inHostPower[i]=(1-utilizationOfHost[i])*P_idle + utilizationOfHost[i]*P_max;
    		powerConsumed+=inHostPower[i];
    	}    	
    	
    	System.out.println("Power consumed till path "+oldPath+" & "+selectedVNode+" is: "+powerConsumed);
    	
    	return powerConsumed;
    }
    
    public boolean end()
    {
        return m_nodesToVisitTbl.isEmpty();
    }

   public static int[] getBestPath()
    {
        int nBestPathArray[] = new int[s_bestPathVect.size()];
        for(int i = 0; i < s_bestPathVect.size(); i++)
        {
            nBestPathArray[i] = ((Integer)s_bestPathVect.elementAt(i)).intValue();
        }

        return nBestPathArray;
    }
        
    public String toString()
    {
        return "Ant " + m_nAntID + ":" + m_nCurNode;
    }
}

