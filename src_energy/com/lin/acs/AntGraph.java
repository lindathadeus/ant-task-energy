/**
 * AntGraph.java
 *
 * @author Created by Omnicore CodeGuide
 * @author Linda J
 */
package com.lin.acs;

import java.io.*;

public class AntGraph implements Serializable
{
    private double[][] m_delta;
    private double[][] m_tau;
    private int        m_nTNodes, m_nVNodes;
    private double     m_dTau0;
    
    public AntGraph(int nTNodes,int nVNodes, double[][] delta, double[][] tau)
    {
       // if(delta.length != nNodes)
         //   throw new IllegalArgumentException("The number of nodes doesn't match with the dimension of delta matrix");
        
        m_nTNodes = nTNodes;
        m_nVNodes = nVNodes;
        m_delta = delta;
        m_tau   = tau;
    }
    
    public AntGraph(int tNodes, int vNodes, double[][] delta)
    {
        this(tNodes, vNodes, delta, new double[tNodes][vNodes]);
        
        resetTau();
    }
    
    public synchronized double delta(int r, int s)
    {
    	//System.out.println("\nr="+r+"; s="+s);
        return m_delta[r][s];
    }
    
    public synchronized double tau(int r, int s)
    {
        return m_tau[r][s];
    }
    
    public synchronized double etha(int r, int s)
    {
        return ((double)1) / delta(r, s);
    }
    
    public synchronized int tNodes()
    {
        return m_nTNodes;
    }
  
    public synchronized int vNodes()
    {
        return m_nVNodes;
    }
      
    public synchronized double tau0()
    {
        return m_dTau0;
    }
    
    public synchronized void updateTau(int r, int s, double value)
    {
        m_tau[r][s] = value;
    }
    
    public void resetTau()
    {
        double dAverage = averageDelta();
        
        m_dTau0 = (double)1 / ( (0.5 * dAverage));
        
        System.out.println("Average: " + dAverage);
        System.out.println("Tau0: " + m_dTau0);
        
        for(int r = 0; r < tNodes(); r++)
        {
            for(int s = 0; s < vNodes(); s++)
            {
                m_tau[r][s] = m_dTau0;
            }
        }
    }
    
    public double averageDelta()
    {
        return average(m_delta);
    }
    
    public double averageTau()
    {
        return average(m_tau);
    }

    public String toString()
    {
        String str = "";
        String str1 = "";
        
        
        for(int r = 0; r < tNodes(); r++)
        {
            for(int s = 0; s < vNodes(); s++)
            {
                str += delta(r,s) + "\t";
                str1 += tau(r,s) + "\t";
            }
            
            str+= "\n";
        }
        
        return str + "\n\n\n" + str1;
    }
    
    private double average(double matrix[][])
    {
        double dSum = 0;
        for(int r = 0; r < m_nTNodes; r++)
        {
            for(int s = 0; s < m_nVNodes; s++)
            {
                dSum += matrix[r][s];
            }
        }
        
        double dAverage = dSum / (double)(m_nTNodes * m_nVNodes);
        
        return dAverage;
    }
}

