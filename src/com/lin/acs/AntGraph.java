/**
 * AntGraph.java
 *
 * @author Created by Omnicore CodeGuide
 * @author Linda J
 */
package com.lin.acs;

import java.io.*;

/**
 * The Class that represents the Graph to be traversed by the ants.
 * @author linda
 *
 */
public class AntGraph implements Serializable
{
	/** Distance Matrix for the graph */
    private double[][] m_delta;
    /** Pheromone Matrix for the graph */
    private double[][] m_tau;
    /** Nodes for the graph */
    private int        m_nTNodes, m_nVNodes;
    /** Initial Phermone Value for the graph */
    private double     m_dTau0;
    
    /**
     * Initializes the Graph
     * @param nTNodes
     * @param nVNodes
     * @param delta
     * @param tau
     */
    public AntGraph(int nTNodes,int nVNodes, double[][] delta, double[][] tau)
    {
       // if(delta.length != nNodes)
         //   throw new IllegalArgumentException("The number of nodes doesn't match with the dimension of delta matrix");
        
        m_nTNodes = nTNodes;
        m_nVNodes = nVNodes;
        m_delta = delta;
        m_tau   = tau;
    }
    
    /**
     * Initializes the Graph
     * @param tNodes
     * @param vNodes
     * @param delta
     */
    public AntGraph(int tNodes, int vNodes, double[][] delta)
    {
        this(tNodes, vNodes, delta, new double[tNodes][vNodes]);
        
        resetTau();
    }
    
    /**
     * Gets the Edge Cost or Distance for r node and s node 
     * @param r
     * @param s
     * @return distance
     */
    public synchronized double delta(int r, int s)
    {
    	//System.out.println("\nr="+r+"; s="+s);
        return m_delta[r][s];
    }
    
    /**
     * Gets the Pheromone value in the edge formed by r node and s node
     * @param r
     * @param s
     * @return Pheromone value
     */
    public synchronized double tau(int r, int s)
    {
        return m_tau[r][s];
    }
    
    /**
     * Desirability value for the edge b/w r and s
     * @param r
     * @param s
     * @return Desirability_value
     */
    public synchronized double etha(int r, int s)
    {
        return ((double)1) / delta(r, s);
    }
    
    /**
     * Gets the total task nodes
     * @return task nodes
     */
    public synchronized int tNodes()
    {
        return m_nTNodes;
    }
  
    /**
     * Gets the total vnodes
     * @return vnodes
     */
    public synchronized int vNodes()
    {
        return m_nVNodes;
    }
      
    /**
     * Gets the initial Pheromone value
     * @return initial Pheromone
     */
    public synchronized double tau0()
    {
        return m_dTau0;
    }
    
    /**
     * Updates the Pheromone in the edge of (r,s) with 
     * that given value
     * @param r
     * @param s
     * @param value
     */
    public synchronized void updateTau(int r, int s, double value)
    {
        m_tau[r][s] = value;
    }
    /**
     * Resets the Pheromone to initial Pheromone Value
     */
    public void resetTau()
    {
        double dAverage = averageDelta();
        
        m_dTau0 = (double)1 / ( (0.5 * dAverage));
        
        System.out.println("Average: " + dAverage);
        System.out.println("Tau0: " + m_dTau0);
        
        for(int r = 0; r < tNodes(); r++)
        {
            for(int s = 0; s < vNodes(); s++)
            {
                m_tau[r][s] = m_dTau0;
            }
        }
    }
    /**
     * Gets the Average value of the Distance Matrix 
     * @return average
     */
    public double averageDelta()
    {
        return average(m_delta);
    }
    
    /**
     * Gets the average value of the Pheromone Matrix
     * @return average Pheromone
     */
    public double averageTau()
    {
        return average(m_tau);
    }

    public String toString()
    {
        String str = "";
        String str1 = "";
        
        
        for(int r = 0; r < tNodes(); r++)
        {
            for(int s = 0; s < vNodes(); s++)
            {
                str += delta(r,s) + "\t";
                str1 += tau(r,s) + "\t";
            }
            
            str+= "\n";
        }
        
        return str + "\n\n\n" + str1;
    }
    /**
     * Returns the Average value in the given matrix
     * @param matrix
     * @return average
     */
    private double average(double matrix[][])
    {
        double dSum = 0;
        for(int r = 0; r < m_nTNodes; r++)
        {
            for(int s = 0; s < m_nVNodes; s++)
            {
                dSum += matrix[r][s];
            }
        }
        
        double dAverage = dSum / (double)(m_nTNodes * m_nVNodes);
        
        return dAverage;
    }
}

