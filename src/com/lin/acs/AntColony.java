/**
 * Pure Java console application.
 * This application demonstrates console I/O.
 *
 * This file was automatically generated by
 * Omnicore CodeGuide.
 */

package com.lin.acs;

import java.util.*;
import java.io.*;

import com.lin.sched.Constant;
/**
 * Ant Colony
 * @author linda
 *
 */

public class AntColony implements Observer
{
    protected PrintStream m_outs;
    
    /** Graph used by the Ants for visiting */
    protected AntGraph m_graph;
    /** Ants */
    protected Ant[]    m_ants;
    /** Total Ants */
    protected int      m_nAnts;
    /** Ant Counter */
    protected int      m_nAntCounter;
    /** Iteration Counter */
    protected int      m_nIterCounter;
    /** Iterations */
    protected int      m_nIterations;
    /** id */
    private int      m_nID;
    /** Counter */
    private static int s_nIDCounter = 0;
    
    //TSP Specific variables
    /** Alpha Parameter for Global Updation Rule */
    protected static final double A = 0.1;
    
    /**
     * Initializes the AntColony
     * @param graph
     * @param nAnts
     * @param nIterations
     */
    public AntColony(AntGraph graph, int nAnts, int nIterations)
    {
        m_graph = graph;
        m_nAnts = nAnts;
        m_nIterations = nIterations;
        s_nIDCounter++;
        m_nID = s_nIDCounter;
    }
    
    /**
     * Starts the AntColony
     */
    public synchronized void start()
    {
        // creates all ants
        m_ants  = createAnts(m_graph, m_nAnts);
        
        m_nIterCounter = 0;
        try
        {
            m_outs = new PrintStream(new FileOutputStream(Constant.path + m_nID + "_" + m_graph.tNodes() + "x" + m_ants.length + "x" + m_nIterations + "_colony.txt"));
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        
        // loop for all iterations
        while(m_nIterCounter < m_nIterations)
        {
            // run an iteration
            iteration();
            try
            {
                wait();
            }
            catch(InterruptedException ex)
            {
                ex.printStackTrace();
            }
            
            // synchronize the access to the graph
            synchronized(m_graph)
            {
                // apply global updating rule
                globalUpdatingRule();
            }
        }
        
        if(m_nIterCounter == m_nIterations)
        {
            m_outs.close();
        }
    }
    
    /**
     * iterates till the end of the process
     */
    private void iteration()
    {
        m_nAntCounter = 0;
        m_nIterCounter++;
        m_outs.print(m_nIterCounter);
        for(int i = 0; i < m_ants.length; i++)
        {
            m_ants[i].start();
        }
    }
    /**
     * Gets the Graph
     * @return AntGraph
     */
    public AntGraph getGraph()
    {
        return m_graph;
    }
    
    /**
     * Gets the Total Ants
     * @return total number of ants
     */
    public int getAnts()
    {
        return m_ants.length;
    }
    
	/**
	 * Gets the Iterations
	 * @return iterations
	 */
    public int getIterations()
    {
        return m_nIterations;
    }
    
    /**
     * Gets the current iteration counter
     * @return iteration
     */
    public int getIterationCounter()
    {
        return m_nIterCounter;
    }
    
    /**
     * Gets the Ant's id
     * @return id
     */
    public int getID()
    {
        return m_nID;
    }
    
    /**
     * Updates the Bestmakespan once Ants find it out.
     */
    public synchronized void update(Observable ant, Object obj)
    {
        //m_outs.print(";" + ((Ant)ant).m_dPathValue);
        m_nAntCounter++;
        
        if(m_nAntCounter == m_ants.length)
        {
            m_outs.println(";" + Ant.s_dBestPathValue + ";" + m_graph.averageTau());
            
                        System.out.println("------------------------------------------------------------------------------------------------------------");
                        System.out.println(m_nIterCounter + " - "+ getBestPathVector()+
                                " Makespan:"+Ant.s_dBestMakeSpanValue+
                                ", Power: " + (Ant.s_dBestPowerValue)/(3600*1000)+
                                "kWh, Power(No Conservation): " + (Ant.s_dBestNoConservationPowerValue)/(3600*1000)
                                +"kWh"
                        		);
                        System.out.println("------------------------------------------------------------------------------------------------------------");
            
            
            
            notify();
            
        }
    }
    
    /**
     * Gets the Best Path Cost value
     * @return best-path's cost
     */
    public double getBestPathValue()
    {
        return Ant.s_dBestPathValue;
    }
    
    /**
     * Gets the Best Path
     * @return Best Path
     */
    public int[] getBestPath()
    {
        return Ant.getBestPath();
    }
    
    /**
     * Gets the Best Path
     * @return Best Path
     */
    public Vector getBestPathVector()
    {
        return Ant.s_bestPathVect;
    }
    
    /**
     * Gets the last best iteration
     * @return iteration
     */
    public int getLastBestPathIteration()
    {
        return Ant.s_nLastBestPathIteration;
    }
    
    /**
     * Returns whether the process is complete
     * @return boolean
     */
    public boolean done()
    {
        return m_nIterCounter == m_nIterations;
    }
    
    /**
     * Creates the Ants
     * @param graph
     * @param nAnts
     * @return Ant[]
     */
    protected Ant[] createAnts(AntGraph graph, int nAnts)
    {
        Random ran = new Random(System.currentTimeMillis());
        Ant.reset();
        Ant.setAntColony(this);
        Ant ant[] = new Ant[nAnts];
        for(int i = 0; i < nAnts; i++)
        {
            ant[i] = new Ant((int)(graph.tNodes() * ran.nextDouble()), this);
        }
        
        return ant;
    }
 
    /**
     * Global Updation Rule for Pheromone
     */
    protected void globalUpdatingRule()
    {
        double dEvaporation = 0;
        double dDeposition  = 0;
        
        for(int r = 0; r < m_graph.tNodes(); r++)
        {
            for(int s = 0; s < m_graph.vNodes(); s++)
            {
                if(r != s)
                {
                    // get the value for deltatau
                    double deltaTau = //Ant4TSP.s_dBestPathValue * (double)Ant4TSP.s_bestPath[r][s];
                        ((double)1 / Ant.s_dBestPathValue) * (double)Ant.s_bestPath[r][s];
                                    
                    // get the value for phermone evaporation as defined in eq. d)
                    dEvaporation = ((double)1 - A) * m_graph.tau(r,s);
                    // get the value for phermone deposition as defined in eq. d)
                    dDeposition  = A * deltaTau;
                    
                    // update tau
                    m_graph.updateTau(r, s, dEvaporation + dDeposition);
                }
            }
        }
    }

}
