package com.lin.tester;

import java.util.*;
import com.lin.sched.*;

import java.io.*;

import com.lin.acs.*;

/**
 * Class for Finding the Job Scheduling Sequence using Bipartite Graph
 * @author Linda J
 *
 */

public class Bipartite
{							
	public static int nTasks = Constant.nTasks;
	public static int nVms = Constant.nVms;
	public static double d[][] = new double[nTasks][nTasks];
	public static double u[] = new double[Constant.nHosts];//utilization list
        
    public static void main(String[] args)
    {
    	//overall printing, comment to print on console
    	//Constant.printToFile("Aco"+nTasks+"x"+nVms+".txt");
    	
        // Print application prompt to console.
        System.out.println("AntColonySystem using Bipartite");
       
        //Input values given here
        int nAnts = Constant.Ants;
        //int nNodes = nTasks;  //+nVms-(nTasks%nVms); //approach 1
        int nIterations = Constant.Iterations;
        
        int nRepetitions = Constant.Repetitions;
        
        double et[][]=new double[nTasks][nVms];
        if(nAnts == 0 || nTasks == 0 || nVms==0 || nIterations == 0 || nRepetitions == 0)
        {
            System.out.println("One of the parameters is wrong");
            
            return;
        }
        
        System.out.println("Ants: " + nAnts+"\nTNodes: " +nTasks+ "\nVNodes: " +nVms+
        		"\nIterations: "+nIterations +"\nRepetitions: "+nRepetitions);
        //OverallOutput.printOutput("Linda TSP using ACO" );
        //Reading ET Matrix from the file
        int et_i=0,et_j=0;
        try{
        	File et_file = new File(Constant.path+"ET_" + nTasks +"x"+nVms+ ".txt");
        	String each_row="";
        	BufferedReader br = new BufferedReader(new FileReader(et_file));
        	
        	while((each_row=br.readLine()) != null){
        		String et_values[]=each_row.split(","); 
        		List<String> list = Arrays.asList(et_values);
        		//Collections.reverse(list);
        		for(et_j=0;et_j<list.size();et_j++)
        			et[et_i][et_j]=Double.parseDouble(list.get(et_j));
        		et_i++;
        	}
        	
        	br.close();
        	
        }catch(Exception e){
        	e.printStackTrace();
        }
        
        System.out.println("ET Matrix");
        //OverallOutput.appendOutput("ET Matrix");
        for(int i = 0; i < nTasks; i++){
            for(int j = 0; j < nVms; j++)
            	System.out.print(" "+et[i][j]);
            System.out.println(" ");
        }
       
        //Reading Utilization list from the file
        int ut_i=0;
        try{
        	File ut_file = new File(Constant.path+"UT_" + Constant.nHosts+ ".txt");
        	String each_row="";
        	BufferedReader br = new BufferedReader(new FileReader(ut_file));
        	
        	while((each_row=br.readLine()) != null){
        		String ut_values=each_row.toString(); 
        		
        		//utilization list
        		u[ut_i]=Double.parseDouble(ut_values);
        		ut_i++;
        	}
        	
        	br.close();
        	
        }catch(Exception e){
        	e.printStackTrace();
        }

        System.out.println("\nUtilization List\n");
        
        for(int i=0;i<u.length;i++)
        	System.out.println(u[i]);
        System.out.println("\n");
        
        double max[] = new double[nVms];
        //initializing the max
        for(int i=0;i<nVms;i++)
        	max[i]=0;
        //take the max values for first round
        for(int i=0;i<nTasks;i++)
        	for(int j=0;j<nVms;j++)
        		if(et[i][j]>max[j])
        			max[j]=et[i][j];
        
        //calculation of distance cum ETC Matrix and EET Matrix Approach
        d = new double[nTasks][nVms];
        
        System.out.println("\nETC Matrix");
        for(int i=0;i<nTasks;i++){
        	for(int j=0;j<nVms;j++)
        	{
        		//int round_j=j/nVms;
        		d[i][j]=et[i][j%nVms];//*/+max[j%nVms]*round_j;
        		System.out.print(d[i][j]+" ");
        	}
        	System.out.print("\n");
        }
         AntGraph graph = new AntGraph(nTasks, nVms, d);
                                
        try
        {
            File graph_file2 = new File(Constant.path + nTasks + "_antgraph.txt");
            if(!graph_file2.exists())
            	graph_file2.createNewFile();
            FileOutputStream outs1 = new FileOutputStream(graph_file2);
            
            for(int i = 0; i < nTasks; i++)
            {
                for(int j = 0; j < nVms; j++)
                {
                    outs1.write((graph.delta(i,j) + ",").getBytes());
                }
                outs1.write('\n');
            }
            
            outs1.close();
        
            PrintStream outs2 = new PrintStream(new FileOutputStream(Constant.path+ nTasks + "x" + nAnts + "x" + nIterations + "_results.txt"));
                         
            for(int i = 0; i < nRepetitions; i++)
            {
                graph.resetTau();
                AntColony antColony = new AntColony(graph, nAnts, nIterations);
                antColony.start();
                outs2.println(antColony.getBestPathValue() + "," + antColony.getLastBestPathIteration());
                System.out.println(Arrays.toString(antColony.getBestPath()));
            }
            outs2.close();
        }
        catch(Exception ex)
        {
        	System.out.print("Exception caught: ");
        	ex.printStackTrace();
        }
    }

}

