package com.lin.tester;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;
import com.lin.sched.Constant;

public class Fcfs {

	/**
	 * @param args
	 */
	
	public static int nTasks = Constant.nTasks;
	public static int nVms = Constant.nVms;
	public static double d[][] = new double[nTasks][nTasks];
	public static double u[] = new double[Constant.nHosts];//utilization list
        /** Path's makespan value 	 */
        protected static double   pathMakeSpanValue; // path's makespan value
        /** Path's power value 	 */
        protected static double   pathPowerValue; // path's power value
        /** Path's power value without conservation	 */
        protected static double   pathNoConservationPowerValue; // path's power value
        /** Task Completion time in Vm	 */
        private static double[]   taskCompletionTimeInVm = new double[Constant.nVms]; // Task Completion time in Vm
        /** Power in Hosts	 */
        private static double[]   powerInHost = new double[Constant.nHosts];
        /** Path traced by scheduler */
        protected static Vector   pathVect = new Vector((nTasks * 2) );        

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

    	//overall printing, comment to print on console
    	//Constant.printToFile("Fcfs"+nTasks+"x"+nVms+".txt");
    	
        // Print application prompt to console.
        System.out.println("\t\tFCFS Energy Management by Linda");
        
        //Reading ET Matrix from the file
        int et_i=0,et_j=0;
        try{
        	File et_file = new File(Constant.path+"ET_" + nTasks +"x"+nVms+ ".txt");
        	String each_row="";
        	BufferedReader br = new BufferedReader(new FileReader(et_file));
        	
        	while((each_row=br.readLine()) != null){
        		String et_values[]=each_row.split(","); 
        		List<String> list = Arrays.asList(et_values);
        		//Collections.reverse(list);
        		for(et_j=0;et_j<list.size();et_j++)
        			d[et_i][et_j]=Double.parseDouble(list.get(et_j));
        		et_i++;
        	}
        	
        	br.close();
        	
        }catch(Exception e){
        	e.printStackTrace();
        }
        
        System.out.println("ET Matrix");
        //OverallOutput.appendOutput("ET Matrix");
        for(int i = 0; i < nTasks; i++){
            for(int j = 0; j < nVms; j++)
            	System.out.print(" "+d[i][j]);
            System.out.println(" ");
        }
       
        //Reading Utilization list from the file
        int ut_i=0;
        try{
        	File ut_file = new File(Constant.path+"UT_" + Constant.nHosts+ ".txt");
        	String each_row="";
        	BufferedReader br = new BufferedReader(new FileReader(ut_file));
        	
        	while((each_row=br.readLine()) != null){
        		String ut_values=each_row.toString(); 
        		
        		//utilization list
        		u[ut_i]=Double.parseDouble(ut_values);
        		ut_i++;
        	}
        	
        	br.close();
        	
        }catch(Exception e){
        	e.printStackTrace();
        }

        System.out.println("\nUtilization List\n");
        
        for(int i=0;i<u.length;i++)
        	System.out.println(u[i]);
        //fcfs schedule
        schedule();
	}
	
	private static void schedule(){
		
		System.out.println("\n\tFCFS Scheduler\n");
    	for(int i=0;i<nTasks;i++){
    		taskCompletionTimeInVm[i%nVms] += d[i][i%nVms];
    		powerInHost[i % nVms] = taskCompletionTimeInVm[i % nVms]*  
            		((1 - u[i % nVms]/100 ) * Constant.P_idle 
        			+ u[i % nVms]/100 * Constant.P_max);
    		
    		pathVect.addElement((Integer)i);pathVect.addElement((Integer)i%nVms);
    		System.out.println("Task"+i+" is bound with VM"+(i%nVms));
    	}
    	
    	//calculate makespan -> max of task Completion time
    	double max=0;
    	for(int k=0;k<Constant.nVms;k++)
    		if(max<taskCompletionTimeInVm[k])
    			max=taskCompletionTimeInVm[k];
    	pathMakeSpanValue=max;
    	
    	//calculate the total power consumed in hosts in that path
    	double totalPowerConsumed = 0;
        for(int i=0;i<Constant.nHosts;i++){
        	//System.out.println("Host["+i+"] = "+inHostPower[i]);
        	totalPowerConsumed += taskCompletionTimeInVm[i] *
                    (((1 - u[i]/100 ) * Constant.P_idle 
    			+ u[i]/100 * Constant.P_max));
        }
        pathPowerValue = totalPowerConsumed;
        
        //calculate the total power consumed in hosts without conservation
        double totalPowerWithoutConservation = 0;
        for(int i=0;i<Constant.nHosts;i++){
            totalPowerWithoutConservation += pathMakeSpanValue * 
                    (((1 - u[i]/100 ) * Constant.P_idle 
    			+ u[i]/100 * Constant.P_max));
            //System.out.println("u["+i+"]="+u[i]);
        }
        pathNoConservationPowerValue = totalPowerWithoutConservation;
 

        System.out.println("------------------------------------------------------------------------------------------------------------");
        System.out.println(" Path: " +pathVect+" Makespan:"+pathMakeSpanValue+
                ", Power: " + (pathPowerValue)/(3600*1000)+
                "kWh, Power(No Conservation): " + (pathNoConservationPowerValue)/(3600*1000)
                +"kWh"
        		);
        System.out.println("------------------------------------------------------------------------------------------------------------");

	}

}
