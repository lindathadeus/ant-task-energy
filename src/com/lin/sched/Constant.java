package com.lin.sched;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

public class Constant {
	
	//relative path windows=".\\output\\"  --> in the current parent directory
	//linux="./output/"
	
	public static String path= ".\\output\\" ;
       
	public static int nTasks = 5;
	public static int nVms = 2;
	public static int nHosts = nVms;
	
	public static double P_max = 250.0;
	public static double P_idle = 0.7 * P_max;
	
	/** Ant's Parameters */
        public static int Ants = 1;
        public static int Iterations = 10;
        public static int Repetitions = 1;
        public static int nNodes = nTasks;
	
        public static double Beta = 1;
        public static double Gamma = 1;
        public static double Q0 = 0.9;
        public static double Rho  = 0.1;
	
	public static void printToFile(String filename) {
	    
		
	       try {
			PrintStream out = new PrintStream(new FileOutputStream(path+filename));
			System.setOut(out);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	       
	}
}
