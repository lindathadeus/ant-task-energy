package com.lin.sched;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.cloudbus.cloudsim.DatacenterCharacteristics;
import org.cloudbus.cloudsim.Pe;
import org.cloudbus.cloudsim.Storage;
import org.cloudbus.cloudsim.VmSchedulerTimeShared;
import org.cloudbus.cloudsim.power.PowerDatacenter;
import org.cloudbus.cloudsim.power.PowerDatacenterNonPowerAware;
import org.cloudbus.cloudsim.power.PowerHost;
import org.cloudbus.cloudsim.power.PowerVmAllocationPolicySimple;
import org.cloudbus.cloudsim.power.models.PowerModelLinear;
import org.cloudbus.cloudsim.provisioners.BwProvisionerSimple;
import org.cloudbus.cloudsim.provisioners.PeProvisionerSimple;
import org.cloudbus.cloudsim.provisioners.RamProvisionerSimple;

public class PowerDatacenterCreator {

	static List<PowerHost> hostList = new ArrayList<PowerHost>();

	static double maxPower = 250; // 250W
	static double staticPowerPercent = 0.7; // 70%

	static int[] mips = { 1000 };
	static int ram = 10000; // host memory (MB)
	static long storage = 1000000; // host storage
	static int bw = 100000;

	String arch = "x86"; // system architecture
	String os = "Linux"; // operating system
	String vmm = "Xen";
	double time_zone = 10.0; // time zone this resource located
	double cost = 3.0; // the cost of using processing in this resource
	double costPerMem = 0.05; // the cost of using memory in this resource
	double costPerStorage = 0.001; // the cost of using storage in this
									// resource
	double costPerBw = 0.0; // the cost of using bw in this resource

	DatacenterCharacteristics characteristics = new DatacenterCharacteristics(
			arch, os, vmm, hostList, time_zone, cost, costPerMem, costPerStorage, costPerBw);

	
	//Create pelist to the Hosts
	void addPEsToHosts(){
		boolean flag=false;
		
		for (int i = 0; i < Constant.nHosts; i++) {
			// 2. A Machine contains one or more PEs or CPUs/Cores.
			// In this example, it will have only one core.
			// 3. Create PEs and add these into an object of PowerPeList.
			
			flag=true;
			List<Pe> peList = new ArrayList<Pe>();
			peList.add(new Pe(0, new PeProvisionerSimple(mips[i % mips.length]))); // need to store PowerPe id and MIPS Rating

			// 4. Create PowerHost with its id and list of PEs and add them to the list of machines
			hostList.add(
				new PowerHost(
					i,
					new RamProvisionerSimple(ram),
					new BwProvisionerSimple(bw),
					storage,
					peList,
					new VmSchedulerTimeShared(peList), new PowerModelLinear(maxPower, staticPowerPercent) // need to store PowerPe id and MIPS Rating

				)
			); // This is our machine
		}
		if(flag)
			System.out.println("ADD PEs executed!!!");

	}
	
	//create datacenter
	public PowerDatacenter createDatacenter(String name) throws Exception{
		
		PowerDatacenter powerDatacenter = null;
		
		//create Hostlist
		addPEsToHosts();
		
		try {
			powerDatacenter = new PowerDatacenterNonPowerAware(
					name,
					characteristics,
					new PowerVmAllocationPolicySimple(hostList),
					new LinkedList<Storage>(),
					5.0);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return powerDatacenter;
	}

	//calculate power consumption in hosts
	public void calculatePowerConsumedInHosts(){
		
		for( int i=0;i<Constant.nHosts;i++){
			System.out.println("Host"+i+" power: "+hostList.get(i).getPower());
			System.out.println("Host"+i+" utilization: "+hostList.get(i).getUtilizationMips());
		}
	}
}
