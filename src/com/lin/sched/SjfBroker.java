package com.lin.sched;
import org.cloudbus.cloudsim.DatacenterBroker;

/**
 * A Broker that schedules Tasks to the VMs 
 * as per Shortest Job First Scheduling Policy
 * @author Linda J
 *
 */
public class SjfBroker extends DatacenterBroker{
   	
	
	public SjfBroker(String name) throws Exception {
		super(name);
		// TODO Auto-generated constructor stub
	}

	//Task length and id pair
	private class TaskLength{
		 int id;
		 long length;
		
		public TaskLength() {
			// TODO Auto-generated constructor stub
			id = 0;
			length = 0;
		}
	}
	
	
	//scheduling function
		public void scheduleTaskstoVms(){
			int reqTasks=cloudletList.size();
			int reqVms=vmList.size();
			int[] orderedTasksId = new int[cloudletList.size()];
			TaskLength[] TaskLenId = new TaskLength[cloudletList.size()];
			
			System.out.println("\n\tSJF Broker Schedules\n");
			
			//initialise ordered tasks id as below
			//task length and id pair
			for(int i=0;i<reqTasks;i++){
				//create objetcs
				TaskLenId[i]=new TaskLength();
				
				orderedTasksId[i]=i;
				TaskLenId[i].id=i;
				TaskLenId[i].length=cloudletList.get(i).getCloudletLength();
			}
			
			//bubble sort based on task length
			TaskLength tmp;
			for(int i=0;i<reqTasks;i++){
				for(int j=0;j<reqTasks-1;j++)
					if(TaskLenId[j].length>TaskLenId[j+1].length){
						tmp=TaskLenId[j];
						
						TaskLenId[j]=TaskLenId[j+1];
						orderedTasksId[j]=TaskLenId[j+1].id;
						
						TaskLenId[j+1]=tmp;
						orderedTasksId[j+1]=tmp.id;
					}
			}
			
			System.out.println("\nTasks arranged based on Tasklength");
			for(int i=0;i<reqTasks;i++)
				System.out.print(" "+TaskLenId[i].id);
			
			System.out.println(" ");
	    	for(int i=0;i<reqTasks;i++){
	    		bindCloudletToVm(TaskLenId[i].id, (i%reqVms));
	    		System.out.println("Task"+cloudletList.get(TaskLenId[i].id).getCloudletId()+" is bound with VM"+vmList.get(i%reqVms).getId());
	    	}
	    	
	    	System.out.println("\n");
		}
}
