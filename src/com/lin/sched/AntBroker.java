package com.lin.sched;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;
import org.cloudbus.cloudsim.DatacenterBroker;
import org.cloudbus.cloudsim.Log;


public class AntBroker extends DatacenterBroker{

	//1,3,2,0,4 ---> best path without makespan approach
	//2, 1, 4, 0, 3, 1, 1, 0, 0, 1
	//5, 0, 9, 2, 8, 1, 7, 2, 6, 1, 4, 0, 3, 2, 2, 1, 1, 0, 0, 2


	private static int[] pathNode = {
        
            2, 1, 4, 0, 3, 1, 1, 0, 0, 1
                
        };
	
        public static int nTasks = Constant.nTasks;
	public static int nVms = Constant.nVms;
	public static double d[][] = new double[nTasks][nTasks];
	public static double u[] = new double[Constant.nHosts];//utilization list
        /** Path's makespan value 	 */
        protected static double   pathMakeSpanValue; // path's makespan value
        /** Path's power value 	 */
        protected static double   pathPowerValue; // path's power value
        /** Path's power value without conservation	 */
        protected static double   pathNoConservationPowerValue; // path's power value
        /** Task Completion time in Vm	 */
        private static double[]   taskCompletionTimeInVm = new double[Constant.nVms]; // Task Completion time in Vm
        /** Power in Hosts	 */
        private static double[]   powerInHost = new double[Constant.nHosts];
        /** Path traced by scheduler */
        protected static Vector   pathVect = new Vector((nTasks * 2) );        
        
        
	public AntBroker(String name) throws Exception {
		super(name);
		// TODO Auto-generated constructor stub
	}

	//schedule function
	public void scheduleTaskstoVms() throws Exception{
		
		int reqTasks=cloudletList.size();
		int reqVms=vmList.size();
                
                            	
        // Print application prompt to console.
        System.out.println("\t\tFCFS Energy Management by Linda");
        
        //Reading ET Matrix from the file
        int et_i=0,et_j=0;
        try{
        	File et_file = new File(Constant.path+"ET_" + nTasks +"x"+nVms+ ".txt");
        	String each_row="";
        	BufferedReader br = new BufferedReader(new FileReader(et_file));
        	
        	while((each_row=br.readLine()) != null){
        		String et_values[]=each_row.split(","); 
        		List<String> list = Arrays.asList(et_values);
        		//Collections.reverse(list);
        		for(et_j=0;et_j<list.size();et_j++)
        			d[et_i][et_j]=Double.parseDouble(list.get(et_j));
        		et_i++;
        	}
        	
        	br.close();
        	
        }catch(Exception e){
        	e.printStackTrace();
        }
        
        System.out.println("ET Matrix");
        //OverallOutput.appendOutput("ET Matrix");
        for(int i = 0; i < nTasks; i++){
            for(int j = 0; j < nVms; j++)
            	System.out.print(" "+d[i][j]);
            System.out.println(" ");
        }
       
        //Reading Utilization list from the file
        int ut_i=0;
        try{
        	File ut_file = new File(Constant.path+"UT_" + Constant.nHosts+ ".txt");
        	String each_row="";
        	BufferedReader br = new BufferedReader(new FileReader(ut_file));
        	
        	while((each_row=br.readLine()) != null){
        		String ut_values=each_row.toString(); 
        		
        		//utilization list
        		u[ut_i]=Double.parseDouble(ut_values);
        		ut_i++;
        	}
        	
        	br.close();
        	
        }catch(Exception e){
        	e.printStackTrace();
        }

        System.out.println("\nUtilization List\n");
        
        for(int i=0;i<u.length;i++)
        	System.out.println(u[i]);
        
	System.out.println("\n\tACO Broker Schedules\n");
    	//bind the cloudlets to the vms. This way, the broker
    	// will submit the bound cloudlets only to the specific VM
    	//based on the path array
                try{
                    if(pathNode.length == 2*reqTasks){
                        System.out.println("Path is Correct!!! :)");
                        for(int i=0;i<2*reqTasks;i+=2){
                            System.out.println(pathNode[i]+", "+pathNode[i+1]+" = "+pathNode[i]+", "+(pathNode[i+1]));
                            bindCloudletToVm(pathNode[i], pathNode[i+1]);
                            taskCompletionTimeInVm[pathNode[i+1]] += d[pathNode[i]][pathNode[i+1]];
                                            
                            powerInHost[pathNode[i+1]] = taskCompletionTimeInVm[pathNode[i+1]]*  
                                        ((1 - u[pathNode[i+1]]/100 ) * Constant.P_idle 
                                                + u[pathNode[i+1]]/100 * Constant.P_max);

                                        }

                    }
                    else throw new Exception();
                }
                catch(Exception e){
                    System.out.println("Path is not Correct!!! :)");
                    throw new Exception();
                }
    	
    	
                         	//calculate makespan -> max of task Completion time
    	double max=0;
    	for(int k=0;k<Constant.nVms;k++)
    		if(max<taskCompletionTimeInVm[k])
    			max=taskCompletionTimeInVm[k];
    	pathMakeSpanValue=max;
    	
    	//calculate the total power consumed in hosts in that path
    	double totalPowerConsumed = 0;
        for(int i=0;i<Constant.nHosts;i++){
        	System.out.println("Host["+i+"] = "+taskCompletionTimeInVm[i]);
        	totalPowerConsumed += taskCompletionTimeInVm[i] *
                    (((1 - u[i]/100 ) * Constant.P_idle 
    			+ u[i]/100 * Constant.P_max));
        }
        pathPowerValue = totalPowerConsumed;
        
        //calculate the total power consumed in hosts without conservation
        double totalPowerWithoutConservation = 0;
        for(int i=0;i<Constant.nHosts;i++){
            totalPowerWithoutConservation += pathMakeSpanValue * 
                    (((1 - u[i]/100 ) * Constant.P_idle 
    			+ u[i]/100 * Constant.P_max));
            System.out.println("u["+i+"]="+u[i]);
        }
        pathNoConservationPowerValue = totalPowerWithoutConservation;
    	Log.printLine(String.format("Energy consumption on datacenter0 without conservation: %.5f kWh ", pathNoConservationPowerValue / (3600 * 1000)));
            Log.printLine(String.format("Energy consumption on datacenter0 with conservation: %.5f kWh ", pathPowerValue / (3600 * 1000)));

	}
        
    public void getPowerInHosts(){
        
        Log.printLine(String.format("Total Makespan: %.5f sec ", pathMakeSpanValue));
        Log.printLine(String.format("Energy consumption on datacenter0 with conservation: %.5f kWh ", pathPowerValue / (3600 * 1000)));
	Log.printLine(String.format("Energy consumption on datacenter0 without conservation: %.5f kWh ", pathNoConservationPowerValue / (3600 * 1000)));
            	
    }
}
